package com.example.tuto.validator;

import com.example.tuto.entity.Producto;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;


public class ProductoPrecioUnidadesMinimoValidator
        implements ConstraintValidator<ProductoPrecioUnidadesMinimo, Producto> {

   @Override
   public boolean isValid(Producto producto, ConstraintValidatorContext context) {
      if (producto.getPrecio() == null || producto.getUnidadesMinimas() == null) {
         return false;
      }

      return producto.getPrecio() * producto.getUnidadesMinimas() > 10;
   }

}